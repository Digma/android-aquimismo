package com.digma.aquimismo.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digma.aquimismo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoTramitesFragment extends Fragment {


    public InfoTramitesFragment() {
        // Required empty public constructor
    }

    public static InfoTramitesFragment newInstance() {
        InfoTramitesFragment fragment = new InfoTramitesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_tramites, container, false);
    }


}
