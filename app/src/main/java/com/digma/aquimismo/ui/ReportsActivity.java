package com.digma.aquimismo.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.digma.aquimismo.R;
import com.digma.aquimismo.adapter.CategoryDenunciaAdapter;
import com.digma.aquimismo.domain.CategoryDenuncia;
import com.digma.aquimismo.helpers.Config;
import com.digma.aquimismo.helpers.MediaHelper;
import com.digma.aquimismo.helpers.PrefHelper;
import com.digma.aquimismo.helpers.Utils;
import com.digma.aquimismo.io.ApiInterface;
import com.digma.aquimismo.io.models.GenericResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

public class ReportsActivity extends AppCompatActivity {

    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    public static final String TYPE_REPORT = "TYPE_REPORT";
    public static final String TYPE_DENUNCIA = "TYPE_DENUNCIA";
    public static final String LATITUD = "LATITUD";
    public static final String LONGITUD = "LONGITUD";
    public static final String TAG = "ReportsActivity";

    private Uri fileUri;
    private int type_report;
    private int type_denuncia;
    private String latitud, longitud;

    private MediaHelper mediaHelper;
    private Utils utils;
    private List<CategoryDenuncia> listDenuncias;

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager linearLayoutManager;
    private VerticalRecyclerViewFastScroller fastScroller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        Bundle extras = getIntent().getExtras();
        type_report = extras.getInt(TYPE_REPORT, -1);
        type_denuncia = extras.getInt(TYPE_DENUNCIA, -1);
        latitud = extras.getString(LATITUD, "0");
        longitud = extras.getString(LONGITUD, "0");

        mediaHelper = new MediaHelper();
        utils = new Utils(getApplicationContext());

        mRecycler = (RecyclerView) findViewById(R.id.recycler_view_category_denuncia);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecycler.setLayoutManager(linearLayoutManager);

        fastScroller = (VerticalRecyclerViewFastScroller) findViewById(R.id.fast_scroller);

        // Connect the recycler to the scroller (to let the scroller scroll the list)
        fastScroller.setRecyclerView(mRecycler);

        // Connect the scroller to the recycler (to let the recycler scroll the scroller's handle)
        mRecycler.setOnScrollListener(fastScroller.getOnScrollListener());

        getDenuncias();
    }

    private void getDenuncias() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        final Call<List<CategoryDenuncia>> categoryDenuncias = apiInterface.getAllCategoryDenuncias();

        categoryDenuncias.enqueue(new Callback<List<CategoryDenuncia>>() {
            @Override
            public void onResponse(Call<List<CategoryDenuncia>> call, Response<List<CategoryDenuncia>> response) {
                listDenuncias = response.body();
                setmAdapter(listDenuncias);
                toggleProgressBar(false);
                fastScroller.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<CategoryDenuncia>> call, Throwable t) {
                t.printStackTrace();
                utils.renderToask(getString(R.string.error_get_category_denuncias), Toast.LENGTH_LONG);
                toggleProgressBar(false);
                toggleBtnReloadDenuncias(true);
            }
        });

    }

    private void setmAdapter(List<CategoryDenuncia> denuncias) {
        if (denuncias == null) denuncias = new ArrayList<>();

        mAdapter = new CategoryDenunciaAdapter(denuncias);

        mRecycler.setAdapter(mAdapter);
    }

    private void toggleBtnReloadDenuncias(boolean show) {
        if (show)
            findViewById(R.id.btn_reload_denuncias).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.btn_reload_denuncias).setVisibility(View.GONE);
    }

    private void toggleProgressBar(boolean show) {
        if (show)
            findViewById(R.id.pbDenuncias).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.pbDenuncias).setVisibility(View.GONE);
    }

    public void onReloadDenuncias(View button) {
        toggleBtnReloadDenuncias(false);
        getDenuncias();
        toggleProgressBar(true);
    }

    /*
    public void onTakePicture(View button) {
        Log.d("Reports", "take Picture");
        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = mediaHelper.getOutputMediaFileUri(mediaHelper.MEDIA_TYPE_IMAGE); // create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }
    */

    public void onSendDenuncia(View button) {
        Log.d("Reports", "send denuncia..");
        if (listDenuncias == null) return;

        for (CategoryDenuncia denuncia : listDenuncias) {
            if (denuncia.isSelected()) {
                if (type_report == 1) {
                    sendDenunciaToServer(denuncia.getId(), "", "");
                    break;
                }

                renderDialog(denuncia.getId());
                /*
                String queja = "", sugerencia = "";
                queja = type_report == 2 ? setQueja() : "";
                sugerencia = type_report == 3 ? setSugerencia() : "";

                sendDenunciaToServer(denuncia.getId(), queja, sugerencia);
                */
                break;
            }
        }
    }

    private String setQueja() {
        return "";
    }

    private String setSugerencia() {
        return "";
    }

    private void renderDialog(final int denuncia_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Escribe tus comentarios..");
        // Set up the input
        final EditText input = new EditText(this);

        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (type_report == 2)
                    sendDenunciaToServer(denuncia_id, input.getText().toString(), "");
                else if (type_report == 3)
                    sendDenunciaToServer(denuncia_id, "", input.getText().toString());
                else
                    sendDenunciaToServer(denuncia_id, "", "");
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void sendDenunciaToServer(int category_denuncia, String pqueja, String psugerencia) {
        //String latitud = "19.844594", longitud = "-90.540239";
        String queja = pqueja, sugerencia = psugerencia;

        int userId = new PrefHelper(this).getUserId(), category = 1, subcategory = category_denuncia;

        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpg");
        MediaType MEDIA_TEXT_PLAIN = MediaType.parse("text/plain");

        File file = null;
        RequestBody bodyImage = null;

        if (fileUri != null) {
            file = new File(fileUri.getPath());
            bodyImage = RequestBody.create(MEDIA_TYPE_PNG, file);
        }

        RequestBody bodyUserId = RequestBody.create(MEDIA_TEXT_PLAIN, userId + "");
        RequestBody bodyTypeReport = RequestBody.create(MEDIA_TEXT_PLAIN, type_report + "");
        RequestBody bodyCategory = RequestBody.create(MEDIA_TEXT_PLAIN, category + "");
        RequestBody bodySubcategory = RequestBody.create(MEDIA_TEXT_PLAIN, subcategory + "");
        RequestBody bodyQueja = RequestBody.create(MEDIA_TEXT_PLAIN, queja);
        RequestBody bodySugerencia = RequestBody.create(MEDIA_TEXT_PLAIN, sugerencia);
        RequestBody bodyLatitud = RequestBody.create(MEDIA_TEXT_PLAIN, latitud);
        RequestBody bodyLongitud = RequestBody.create(MEDIA_TEXT_PLAIN, longitud);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.URL_BASE)
                //.client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<GenericResponse> denuncia = null;

        if (fileUri != null)
            denuncia = apiInterface
                .createDenuncia(bodyUserId, bodyTypeReport, bodyCategory, bodySubcategory, bodySugerencia,
                        bodyQueja, bodyLatitud, bodyLongitud, bodyImage);
        else
            denuncia = apiInterface
                    .createDenuncia(bodyUserId, bodyTypeReport, bodyCategory, bodySubcategory, bodySugerencia,
                            bodyQueja, bodyLatitud, bodyLongitud);

        denuncia.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                Log.d(TAG, response.code() + "");
                String message = response.body().getMessage();

                if (message.toLowerCase().equals("ok")) {
                    utils.renderToask(getString(R.string.created_denuncia), Toast.LENGTH_LONG);
                    return;
                }

                utils.renderToask(message, Toast.LENGTH_LONG);
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (fileUri == null) {
                    utils.renderToask(getString(R.string.file_error), Toast.LENGTH_LONG);
                    return;
                }
                // Image captured and saved to fileUri specified in the Intent
                ImageView imageView = (ImageView) findViewById(R.id.img_camera_picture);
                imageView.setImageURI(fileUri);
                imageView.setVisibility(View.VISIBLE);

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reportes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.action_take_picture:
                Log.d("Reports", "take Picture");
                // create Intent to take a picture and return control to the calling application
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                fileUri = mediaHelper.getOutputMediaFileUri(mediaHelper.MEDIA_TYPE_IMAGE); // create a file to save the image
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

                // start the image capture Intent
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                return true;
            case R.id.action_send_denuncia:
                if (listDenuncias == null) return true;

                for (CategoryDenuncia denuncia : listDenuncias) {
                    if (denuncia.isSelected()) {
                        if (type_report == 1) {
                            sendDenunciaToServer(denuncia.getId(), "", "");
                            break;
                        }

                        renderDialog(denuncia.getId());
                /*
                String queja = "", sugerencia = "";
                queja = type_report == 2 ? setQueja() : "";
                sugerencia = type_report == 3 ? setSugerencia() : "";

                sendDenunciaToServer(denuncia.getId(), queja, sugerencia);
                */
                        break;
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
