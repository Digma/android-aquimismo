package com.digma.aquimismo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.digma.aquimismo.R;
import com.digma.aquimismo.domain.Notificacion;
import com.digma.aquimismo.helpers.Config;
import com.digma.aquimismo.helpers.MapHelpers;
import com.digma.aquimismo.helpers.Utils;
import com.digma.aquimismo.io.ApiInterface;
import com.digma.aquimismo.ui.fragments.InfoTramitesFragment;
import com.digma.aquimismo.ui.fragments.MapsFragment;
import com.digma.aquimismo.ui.fragments.MisDenunciasFragment;
import com.digma.aquimismo.ui.fragments.NotificacionesFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private List<Notificacion> notificacions;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        utils = new Utils(getApplicationContext());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment fragment = MapsFragment.newInstance();
        startFragment(fragment);

        navigationView.getMenu().getItem(0).setChecked(true);

        getNotifications();
    }

    public void onCloseBanner(View button) {
        closeBanner();
    }

    private void closeBanner() {
        findViewById(R.id.btn_close_banner_main).setVisibility(View.GONE);
        findViewById(R.id.img_banner_main).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            Fragment mapsFragment = MapsFragment.newInstance();
            startFragment(mapsFragment);
        } else if (id == R.id.nav_denuncia) {
            Fragment misDenuncias = MisDenunciasFragment.newInstance();
            startFragment(misDenuncias);
        } else if (id == R.id.nav_notificaciones) {
            Fragment notificacionesFragment = NotificacionesFragment.newInstance(notificacions);
            startFragment(notificacionesFragment);
        } else if (id == R.id.nav_info_tramites) {
            Fragment infoTramites = InfoTramitesFragment.newInstance();
            startFragment(infoTramites);
        } else if (id == R.id.nav_my_acount) {
            //Fragment infoTramites = InfoTramitesFragment.newInstance();
            //startFragment(infoTramites);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getNotifications() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        final Call<List<Notificacion>> notificacionCall = apiInterface.getAllNotifications();

        notificacionCall.enqueue(new Callback<List<Notificacion>>() {
            @Override
            public void onResponse(Call<List<Notificacion>> call, Response<List<Notificacion>> response) {
                notificacions = response.body();
                if (notificacions == null) return;

                String message = String.format("%s %d",
                        getString(R.string.notifications_count), notificacions.size());

                utils.renderToask(message, Toast.LENGTH_LONG);
            }

            @Override
            public void onFailure(Call<List<Notificacion>> call, Throwable t) {
                t.printStackTrace();
                utils.renderToask(getString(R.string.error_get_notifications), Toast.LENGTH_LONG);
            }
        });
    }


    private void startFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .commit();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case MapHelpers.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("result", "User agreed to make required location settings changes.");
                        //if (mapHelper != null) mapHelper.moveMapToMyCurrentLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("result", "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

}
