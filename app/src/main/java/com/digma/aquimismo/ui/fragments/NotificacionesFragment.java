package com.digma.aquimismo.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.digma.aquimismo.R;
import com.digma.aquimismo.adapter.NotificacionAdapter;
import com.digma.aquimismo.domain.Notificacion;
import com.digma.aquimismo.helpers.Config;
import com.digma.aquimismo.io.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificacionesFragment extends Fragment {

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager linearLayoutManager;
    private List<Notificacion> notificacions;

    private ProgressBar pbNotifications;
    private Button btnReloadNotifications;

    public NotificacionesFragment(List<Notificacion> notificacions) {
        if (notificacions != null)
            this.notificacions = notificacions;
        else
            this.notificacions = new ArrayList<>();
    }

    public NotificacionesFragment() {
        // Required empty public constructor
    }

    public static NotificacionesFragment newInstance(List<Notificacion> notificacions) {
        NotificacionesFragment fragment = new NotificacionesFragment(notificacions);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificaciones, container, false);

        toggleFabButton(false);

        mRecycler = (RecyclerView) view.findViewById(R.id.recycler_view_notificaciones);
        pbNotifications = (ProgressBar) view.findViewById(R.id.pbNotifications);
        btnReloadNotifications = (Button) view.findViewById(R.id.btn_reload_notifications);

        linearLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(linearLayoutManager);


        setmAdapter(notificacions);

        checkNotifications();

        btnReloadNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkNotifications();
            }
        });

        return view;
    }

    private void checkNotifications() {
        if (notificacions != null && notificacions.size() > 0) return;

        toggleProgressBar(true);
        toggleBtnReloadDenuncias(false);
        getNotifications();
    }

    private void getNotifications() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        final Call<List<Notificacion>> notificacionCall = apiInterface.getAllNotifications();

        notificacionCall.enqueue(new Callback<List<Notificacion>>() {
            @Override
            public void onResponse(Call<List<Notificacion>> call, Response<List<Notificacion>> response) {
                notificacions = response.body();

                setmAdapter(notificacions);

                toggleProgressBar(false);

            }

            @Override
            public void onFailure(Call<List<Notificacion>> call, Throwable t) {
                t.printStackTrace();
                toggleProgressBar(false);
                toggleBtnReloadDenuncias(true);
            }
        });
    }

    private void toggleFabButton(boolean show) {
        if (show)
            getActivity().findViewById(R.id.fab).setVisibility(View.VISIBLE);
        else
            getActivity().findViewById(R.id.fab).setVisibility(View.GONE);
    }

    private void toggleBtnReloadDenuncias(boolean show) {
        if (show)
            btnReloadNotifications.setVisibility(View.VISIBLE);
        else
            btnReloadNotifications.setVisibility(View.GONE);
    }

    private void toggleProgressBar(boolean show) {
        if (show)
            pbNotifications.setVisibility(View.VISIBLE);
        else
            pbNotifications.setVisibility(View.GONE);
    }

    private void setmAdapter(List<Notificacion> notificacions) {
        mAdapter = new NotificacionAdapter(notificacions);

        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        toggleFabButton(true);
    }
}
