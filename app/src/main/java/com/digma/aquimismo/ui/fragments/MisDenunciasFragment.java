package com.digma.aquimismo.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digma.aquimismo.R;
import com.digma.aquimismo.adapter.MiDenunciaAdapter;
import com.digma.aquimismo.domain.MiDenuncia;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisDenunciasFragment extends Fragment {

    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager linearLayoutManager;

    public MisDenunciasFragment() {
        // Required empty public constructor
    }

    public static MisDenunciasFragment newInstance() {
        MisDenunciasFragment fragment = new MisDenunciasFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mis_denuncias, container, false);

        mRecycler = (RecyclerView) view.findViewById(R.id.recycler_view_type_denuncias);

        linearLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(linearLayoutManager);

        List<MiDenuncia> denuncias = new ArrayList<>();

        denuncias.add(new MiDenuncia(1, "Muchos baches."));
        denuncias.add(new MiDenuncia(2, "No hay luz."));
        denuncias.add(new MiDenuncia(3, "Semaforo no sirve."));
        denuncias.add(new MiDenuncia(4, "Hubo un asalto."));

        setmAdapter(denuncias);

        return view;
    }

    private void setmAdapter(List<MiDenuncia> denuncias) {
        mAdapter = new MiDenunciaAdapter(denuncias);

        mRecycler.setAdapter(mAdapter);
    }

}
