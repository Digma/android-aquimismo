package com.digma.aquimismo.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 3/29/16.
 */
public class CategoryDenuncia {

    @SerializedName("id")
    private int id;

    @SerializedName("denuncia")
    private String denuncia;

    private boolean selected;

    public CategoryDenuncia() {
    }

    public CategoryDenuncia(int id, String denuncia) {
        this.id = id;
        this.denuncia = denuncia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDenuncia() {
        return denuncia;
    }

    public void setDenuncia(String denuncia) {
        this.denuncia = denuncia;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
