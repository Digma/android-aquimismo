package com.digma.aquimismo.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 3/29/16.
 */
public class Notificacion {

    @SerializedName("id")
    private int id;

    @SerializedName("notificacion")
    private String notificacion;

    public Notificacion() {
    }

    public Notificacion(int id, String notificacion) {
        this.id = id;
        this.notificacion = notificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(String notificacion) {
        this.notificacion = notificacion;
    }
}
