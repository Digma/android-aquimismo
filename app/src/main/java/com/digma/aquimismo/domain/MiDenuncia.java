package com.digma.aquimismo.domain;

/**
 * Created by janidham on 3/24/16.
 */
public class MiDenuncia {

    private int id;
    private String name;
    private int img_id;

    public MiDenuncia(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }
}
