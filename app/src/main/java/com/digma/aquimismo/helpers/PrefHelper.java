package com.digma.aquimismo.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by janidham on 28/01/16.
 */
public class PrefHelper {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared pref file name
    private static final String PREF_NAME = "AquiMismo";

    // All Shared Preferences Keys of Tutorial
    public static final String DONE_TUTORIAL = "DONE_TUTORIAL";

    // All Shared Preferences Keys of UserAPP
    public static final String USER_ID = "USER_ID";
    public static final String USER_EMAIL = "USER_EMAIL";

    // Constructor
    public PrefHelper(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setDoneTutorial(boolean done) {
        editor.putBoolean(DONE_TUTORIAL, done);
        editor.commit();
    }

    public void setUserApp(int id, String email) {
        editor.putInt(USER_ID, id);
        editor.putString(USER_EMAIL, email);
        editor.commit();
    }

    public boolean getDoneTutorial() { return pref.getBoolean(DONE_TUTORIAL, false); }

    public int getUserId() { return pref.getInt(USER_ID, -1); }

    public String getUserEmail() { return pref.getString(USER_EMAIL, ""); }

}
