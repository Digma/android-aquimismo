package com.digma.aquimismo.helpers;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.digma.aquimismo.ui.fragments.MapsFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by janidham on 05/02/16.
 */
public class MapHelpers
        implements LocationListener,
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult> {

    public static final String TAG = "MapHelpers";

    private GoogleMap mMap;
    private Context mContext;

    GoogleApiClient mGoogleApiClient;

    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    private Location mLastLocation;

    public MapHelpers(GoogleMap mMap, Context context) {
        this.mMap = mMap;
        this.mContext = context;

        checkPlayServices();
    }

    public void disconectGoogleApi() {
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    public void connectGoogleApi() {
        if (mGoogleApiClient != null || !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    public Location getMyLocation() {
        return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    public void moveMapToMyCurrentLocation() {
        if (mMap == null) return;

        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) return;

        mLastLocation = getMyLocation();

        if (mLastLocation == null) return;

        mMap.animateCamera(
                CameraUpdateFactory
                        .newLatLng(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();
    }


    /**
     * Method to verify google play services on the device
     * */
    public void checkPlayServices() {
        if (mGoogleApiClient == null) buildGoogleApiClient();

        LocationRequest mLocationRequest = new LocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        LocationSettingsRequest mLocationSettingsRequest = builder.build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );

        result.setResultCallback(this);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    &&
                    mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return false;
            }
        }
        return true;
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "location changed");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "status changed");
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "conectado..");

        if (!isPermissionGranted()) return;

        moveMapToMyCurrentLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "conexion suspendida..");
        connectGoogleApi();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "conexion fallida");
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                //if (!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(MapsFragment.activity, REQUEST_CHECK_SETTINGS);

                    //status.startResolutionForResult(MainActivity.activity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

}
