package com.digma.aquimismo.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.digma.aquimismo.R;
import com.digma.aquimismo.ui.ReportsActivity;

/**
 * Created by janidham on 3/10/16.
 */
public class Utils {

    private Context context;
    public int indexReport;
    public int indexDenuncia;

    public Utils(Context context) {
        this.context = context;
    }


    public void renderToask(String message, int time) {
        Toast.makeText(context, message, time).show();
    }

    public void renderTypeReport(final String latitud, final String longitud) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.dialog_type_report)
                .setSingleChoiceItems(R.array.array_type_report, -1,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                indexReport = item + 1;
                            }
                        }
                ).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                        if (indexReport == 0) {
                            renderTypeDenuncia();
                            return;
                        }
                        */
                        Intent intent = new Intent(context, ReportsActivity.class);
                        intent.putExtra(ReportsActivity.TYPE_REPORT, indexReport);
                        intent.putExtra(ReportsActivity.LATITUD, latitud);
                        intent.putExtra(ReportsActivity.LONGITUD, longitud);

                        context.startActivity(intent);

                        dialog.dismiss();
                    }
                }
        ).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );

        builder.create();
        builder.show();
    }

    public void renderTypeDenuncia() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.dialog_type_denuncia)
                .setSingleChoiceItems(R.array.array_type_denuncia, -1,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                indexDenuncia = item;
                            }
                        }
                ).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, ReportsActivity.class);
                        intent.putExtra(ReportsActivity.TYPE_REPORT, indexReport);
                        intent.putExtra(ReportsActivity.TYPE_DENUNCIA, indexDenuncia);

                        context.startActivity(intent);

                        dialog.dismiss();
                    }
                }
        ).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );

        builder.create();
        builder.show();
    }

    public boolean isEmailValid(String email) {
        return email.contains("@");
    }

    public boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}
