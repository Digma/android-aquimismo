package com.digma.aquimismo.io;

import com.digma.aquimismo.domain.CategoryDenuncia;
import com.digma.aquimismo.domain.MiDenuncia;
import com.digma.aquimismo.domain.Notificacion;
import com.digma.aquimismo.domain.User;
import com.digma.aquimismo.io.models.GenericResponse;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by janidham on 3/10/16.
 */
public interface ApiInterface {

    @GET("denuncias")
    Call<GenericResponse> getAllDenuncias();

    @GET("notifications")
    Call<List<Notificacion>> getAllNotifications();

    @GET("allcomplaints")
    Call<List<CategoryDenuncia>> getAllCategoryDenuncias();

    @Multipart
    @POST("complaint")
    Call<GenericResponse> createDenuncia(@Part("userapp_id") RequestBody userId,
                                         @Part("type_report") RequestBody type_report,
                                         @Part("category_report") RequestBody category_report,
                                         @Part("catalogodenuncia_id") RequestBody subcategory_report,
                                         @Part("sugerencia") RequestBody sugerencia,
                                         @Part("queja") RequestBody queja,
                                         @Part("latitud") RequestBody latitud,
                                         @Part("longitud") RequestBody longitud,
                                         @Part("foto")RequestBody image);

    @Multipart
    @POST("complaint")
    Call<GenericResponse> createDenuncia(@Part("userapp_id") RequestBody userId,
                                         @Part("type_report") RequestBody type_report,
                                         @Part("category_report") RequestBody category_report,
                                         @Part("catalogodenuncia_id") RequestBody subcategory_report,
                                         @Part("sugerencia") RequestBody sugerencia,
                                         @Part("queja") RequestBody queja,
                                         @Part("latitud") RequestBody latitud,
                                         @Part("longitud") RequestBody longitud);

    @FormUrlEncoded
    @POST("userapp")
    Call<User> createUser(@Field("email") String email, @Field("password") String password);
}
