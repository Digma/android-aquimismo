package com.digma.aquimismo.io.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by janidham on 3/10/16.
 */
public class GenericResponse {

    @SerializedName("message")
    String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
