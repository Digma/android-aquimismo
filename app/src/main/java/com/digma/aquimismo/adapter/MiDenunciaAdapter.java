package com.digma.aquimismo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digma.aquimismo.R;
import com.digma.aquimismo.domain.MiDenuncia;

import java.util.List;

/**
 * Created by janidham on 3/24/16.
 */
public class MiDenunciaAdapter extends RecyclerView.Adapter<MiDenunciaAdapter.DenunciaViewHolder> {

    private List<MiDenuncia> denuncias;

    public MiDenunciaAdapter(List<MiDenuncia> denuncias) {
        this.denuncias = denuncias;
    }

    public static class DenunciaViewHolder extends RecyclerView.ViewHolder {

        ImageView imgDenunciaIcon;
        TextView txtDenunciaName;

        public DenunciaViewHolder(View itemView) {
            super(itemView);

            txtDenunciaName = (TextView) itemView.findViewById(R.id.txt_denuncia_name);
            imgDenunciaIcon = (ImageView) itemView.findViewById(R.id.img_denuncia_icon);
        }
    }

    @Override
    public DenunciaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_mi_denuncia, parent, false);

        return new DenunciaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DenunciaViewHolder holder, int position) {
        final MiDenuncia miDenuncia = denuncias.get(position);

        holder.txtDenunciaName.setText(miDenuncia.getName());
    }

    @Override
    public int getItemCount() {
        return this.denuncias.size();
    }

}
