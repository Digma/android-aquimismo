package com.digma.aquimismo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digma.aquimismo.R;
import com.digma.aquimismo.domain.CategoryDenuncia;
import com.digma.aquimismo.domain.MiDenuncia;

import java.util.List;

/**
 * Created by janidham on 3/24/16.
 */
public class CategoryDenunciaAdapter extends RecyclerView.Adapter<CategoryDenunciaAdapter.DenunciaViewHolder> {

    private List<CategoryDenuncia> denuncias;
    private RadioButton lastChecked = null;
    private int lastCheckedPos = 0;

    public CategoryDenunciaAdapter(List<CategoryDenuncia> denuncias) {
        this.denuncias = denuncias;
    }

    public static class DenunciaViewHolder extends RecyclerView.ViewHolder {

        ImageView imgDenunciaIcon;
        TextView txtDenunciaName;
        RadioButton checkBoxDenuncia;

        public DenunciaViewHolder(View itemView) {
            super(itemView);

            txtDenunciaName = (TextView) itemView.findViewById(R.id.txt_category_denuncia_name);
            imgDenunciaIcon = (ImageView) itemView.findViewById(R.id.img_category_denuncia_icon);
            checkBoxDenuncia = (RadioButton) itemView.findViewById(R.id.check_denuncia);
        }
    }

    @Override
    public DenunciaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_category_denuncia, parent, false);

        return new DenunciaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DenunciaViewHolder holder, int position) {
        final CategoryDenuncia denuncia = denuncias.get(position);

        holder.txtDenunciaName.setText(denuncia.getDenuncia());
        holder.checkBoxDenuncia.setChecked(denuncia.isSelected());
        holder.checkBoxDenuncia.setTag(new Integer(position));

        if (position == 0 && denuncias.get(0).isSelected() && holder.checkBoxDenuncia.isChecked()) {
            lastChecked = holder.checkBoxDenuncia;
            lastCheckedPos = 0;
        }

        holder.checkBoxDenuncia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton cb = (RadioButton) v;
                int clickedPos = ((Integer)cb.getTag()).intValue();

                if(cb.isChecked())
                {
                    if(lastChecked != null)
                    {
                        lastChecked.setChecked(false);
                        denuncias.get(lastCheckedPos).setSelected(false);
                    }

                    lastChecked = cb;
                    lastCheckedPos = clickedPos;
                }
                else
                    lastChecked = null;

                denuncias.get(clickedPos).setSelected(cb.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.denuncias.size();
    }

}
