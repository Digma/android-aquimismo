package com.digma.aquimismo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digma.aquimismo.R;
import com.digma.aquimismo.domain.Notificacion;

import java.util.List;

/**
 * Created by janidham on 3/24/16.
 */
public class NotificacionAdapter extends RecyclerView.Adapter<NotificacionAdapter.NotificacionViewHolder> {

    private List<Notificacion> notificacions;

    public NotificacionAdapter(List<Notificacion> notificacions) {
        this.notificacions = notificacions;
    }

    public static class NotificacionViewHolder extends RecyclerView.ViewHolder {

        ImageView imgNotificacionIcon;
        TextView txtDenunciaName;

        public NotificacionViewHolder(View itemView) {
            super(itemView);

            txtDenunciaName = (TextView) itemView.findViewById(R.id.txt_notificacion_name);
            imgNotificacionIcon = (ImageView) itemView.findViewById(R.id.img_notificacion_icon);
        }
    }

    @Override
    public NotificacionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_notificacion, parent, false);

        return new NotificacionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificacionViewHolder holder, int position) {
        final Notificacion notificacion = notificacions.get(position);

        holder.txtDenunciaName.setText(notificacion.getNotificacion());
    }

    @Override
    public int getItemCount() {
        return this.notificacions.size();
    }

}
